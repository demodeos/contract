<?php
declare(strict_types=1);

namespace Demodeos\BitrixExchange\DTO;

class ProductModel
{
    public $guid;
    public $code;
    public $shortcode;
    public $article;
    public $name;
    public $description;
    public $manufacturer;
    public $nds;
    public $category;
    public $cef_name;

}