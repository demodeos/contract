<?php
declare(strict_types=1);

namespace Demodeos\BitrixExchange\DTO;

class ProductPropertyModel
{
    public $product;
    public $property;
    public $value;
    public $property_name;
    public $value_name;


}