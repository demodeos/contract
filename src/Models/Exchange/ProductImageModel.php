<?php
declare(strict_types=1);

namespace Demodeos\BitrixExchange\DTO;

class ProductImageModel
{
    public $product;
    public $file;
    public $visible;
    public $created_at;

}