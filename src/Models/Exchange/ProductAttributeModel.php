<?php
declare(strict_types=1);

namespace Demodeos\BitrixExchange\DTO;

class ProductAttributeModel
{
    public $product;
    public $attribute;
    public $value;
    public $attribute_name;
    public $value_name;
}