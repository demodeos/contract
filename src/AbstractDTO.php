<?php
declare(strict_types=1);

namespace Demodeos\Contract;

use ReflectionClass;
use ReflectionProperty;

class AbstractDTO
{



    public function load(array|object $data): static
    {
        $reflection = new ReflectionClass($this);

        if(is_array($data)) {
            foreach ($reflection->getProperties(ReflectionProperty::IS_PUBLIC) as $property)
                if (isset($data[$property->name]))
                    $this->{$property->name} = $data[$property->name];
        }
         elseif (is_object($data))
         {
             foreach ($reflection->getProperties(ReflectionProperty::IS_PUBLIC) as $property)
                 if (isset($data->{$property->name}))
                     $this->{$property->name} = $data->{$property->name};

         }
        return $this;
    }

}