<?php return array(
    'root' => array(
        'pretty_version' => 'dev-0.0.3.7',
        'version' => '0.0.3.7-dev',
        'type' => 'library',
        'install_path' => __DIR__ . '/../../',
        'aliases' => array(),
        'reference' => 'c2b96ac6c103882386bf8c7a8c2ceef03672a02f',
        'name' => 'demodeos/contract',
        'dev' => true,
    ),
    'versions' => array(
        'demodeos/contract' => array(
            'pretty_version' => 'dev-0.0.3.7',
            'version' => '0.0.3.7-dev',
            'type' => 'library',
            'install_path' => __DIR__ . '/../../',
            'aliases' => array(),
            'reference' => 'c2b96ac6c103882386bf8c7a8c2ceef03672a02f',
            'dev_requirement' => false,
        ),
    ),
);
